/*******************************************************************************
 SPI Driver:  Implementation

 File Name:
 spi.c

 Summary:
 Implementation of MCU specific SPI functions.

 Description:
 None
 *******************************************************************************/

// Included Files
#include "spi_0.h"
#include "spi.h"

// *****************************************************************************

SI_SEGMENT_VARIABLE(SPI_TxBuf[MAX_BUFFER_SIZE+1], uint8_t,
                    EFM8PDL_SPI0_TX_SEGTYPE);
SI_SEGMENT_VARIABLE(SPI_RxBuf[MAX_BUFFER_SIZE+1], uint8_t,
                    EFM8PDL_SPI0_RX_SEGTYPE);

// *****************************************************************************

// SPI Functions

uint8_t
SPI_ReadByte (uint8_t Reg_addr)
{

  uint8_t dummy=0xFF;
  SPI_TxBuf[0] = CLIENT_ADDR_RD;
  SPI_TxBuf[1] = Reg_addr;

  // Send dummy byte
  SPI_TxBuf[2] = dummy;

  SPI0_transfer (SPI_TxBuf, SPI_RxBuf, SPI0_TRANSFER_RXTX, 3);

  return SPI_RxBuf[2];

}

void
SPI_ReadArray (uint8_t Reg_addr, uint8_t Byte_RD, uint8_t *RxData)
{
  uint8_t i, dummy = 0xFF;
  SPI_TxBuf[0] = CLIENT_ADDR_RD;
  SPI_TxBuf[1] = Reg_addr;

  // Send dummy bytes to shift in RX data
  for (i = 0; i < Byte_RD; i++)
    {
      SPI_TxBuf[2 + i] = dummy;
    }

  SPI0_transfer (SPI_TxBuf, SPI_RxBuf, SPI0_TRANSFER_RXTX, Byte_RD+2);

  for (i = 0; i < Byte_RD; i++)
    {
      //Updating Data
      RxData[i] = SPI_RxBuf[2+i];
    }
}

void
SPI_WriteByte (uint8_t Reg_addr, uint8_t Txdata)
{
  SPI_TxBuf[0] = CLIENT_ADDR_WR;
  SPI_TxBuf[1] = Reg_addr;
  SPI_TxBuf[2] = Txdata;

  SPI0_transfer (SPI_TxBuf, NULL, SPI0_TRANSFER_TX, 3);

}

void
SPI_WriteArray (uint8_t Reg_addr, uint8_t Byte_WR, uint8_t *Txdata)
{
  uint8_t i;
  SPI_TxBuf[0] = CLIENT_ADDR_WR;
  SPI_TxBuf[1] = Reg_addr;
  for (i = 0; i < Byte_WR; i++)
    {
      // Updating I2C bus
      SPI_TxBuf[i] = Txdata[i];
    }

  SPI0_transfer (SPI_TxBuf, NULL, SPI0_TRANSFER_TX, Byte_WR + 2);
}

