/*******************************************************************************
 MCP23S08 Driver:  Header File

 File Name:
 mcp23s08.h

 Summary:
 This header file contains object declarations used in the API.
 This also contains device specific defines and register address defines.
 This header file provides the Register Level Function Prototypes for the
 MCP23S08 I/O Expander.

 Description:
 .
 *******************************************************************************/

#ifndef SRC_MCP23S08_H_
#define SRC_MCP23S08_H_

// *****************************************************************************

// Included Files

#include <stdint.h>

// *****************************************************************************

//  Register Addresses

#define MCP23S08_IODIR_A        0x00    //  Data Direction Register A
#define MCP23S08_IPOL_A         0x01    //  Input Polarity A
#define MCP23S08_GPINTEN_A      0x02    //  NOT USED interrupt enable
#define MCP23S08_DEFVAL_A       0x03    //  NOT USED interrupt def
#define MCP23S08_INTCON_A       0x04    //  NOT USED interrupt control
#define MCP23S08_IOCON_A        0x05    //  IO control register
#define MCP23S08_GPPU_A         0x06    //  Pull Up Resistors A
#define MCP23S08_INTF_A         0x07    //  NOT USED interrupt flag
#define MCP23S08_INTCAP_A       0x08    //  NOT USED interrupt capture
#define MCP23S08_GPIO_A         0x09    //  General Purpose IO A
#define MCP23S08_OLAT_A         0x0A    //  NOT USED output latch

// *****************************************************************************

//Object Declarations

//! I/O DIRECTION Register

typedef struct _REG_IODIR {

        uint8_t IO0 : 1;
        uint8_t IO1 : 1;
        uint8_t IO2 : 1;
        uint8_t IO3 : 1;
        uint8_t IO4 : 1;
        uint8_t IO5 : 1;
        uint8_t IO6 : 1;
        uint8_t IO7 : 1;

} REG_IODIR;

//! Pin Status

typedef struct _PIN_STATUS{

        uint8_t PinMode : 1;
        uint8_t Polarity : 1;
        uint8_t INTR_CHG : 1;
        uint8_t DEFVAL : 1;
        uint8_t INTCON : 1;
        uint8_t GPPU : 1;
        uint8_t GPIO : 1;
        uint8_t OLAT : 1;


} PIN_STATUS;

// I/O DIRECTION Register

typedef enum REG_IODIR {

        IO0 = 0xfe,
        IO1 = 0xfd,
        IO2 = 0xfb,
        IO3 = 0xf7,
        IO4 = 0xef,
        IO5 = 0xdf,
        IO6 = 0xbf,
        IO7 = 0x7F

} REG_IO;

// – INPUT POLARITY PORT REGISTER

typedef enum REG_IPOL {

        IP0 = 0x01,
        IP1 = 0x02,
        IP2 = 0x04,
        IP3 = 0x08,
        IP4 = 0x10,
        IP5 = 0x20,
        IP6 = 0x40,
        IP7 = 0x80

} REG_IPOL;

// – INTERRUPT-ON-CHANGE CONTROL REGISTER

typedef enum REG_GPINT {

  GPINT0 = 0x01,
  GPINT1 = 0x02,
  GPINT2 = 0x04,
  GPINT3 = 0x08,
  GPINT4 = 0x10,
  GPINT5 = 0x20,
  GPINT6 = 0x40,
  GPINT7 = 0x80

} REG_GPINT;

// DEFAULT VALUE REGISTER

typedef enum REG_DEF {

  DEF0 = 0x01,
  DEF1 = 0x02,
  DEF2 = 0x04,
  DEF3 = 0x08,
  DEF4 = 0x10,
  DEF5 = 0x20,
  DEF6 = 0x40,
  DEF7 = 0x80

} REG_DEF;

// INTERRUPT-ON-CHANGE CONTROL REGISTER

typedef enum REG_INTCON {

  INTCON0 = 0x01,
  INTCON1 = 0x02,
  INTCON2 = 0x04,
  INTCON3 = 0x08,
  INTCON4 = 0x10,
  INTCON5 = 0x20,
  INTCON6 = 0x40,
  INTCON7 = 0x80

} REG_INTCON;

// GPIO PULL-UP RESISTOR REGISTER

typedef enum REG_GPPU {

  PU0 = 0x01,
  PU1 = 0x02,
  PU2 = 0x04,
  PU3 = 0x08,
  PU4 = 0x10,
  PU5 = 0x20,
  PU6 = 0x40,
  PU7 = 0x80

} REG_GPPU;

// GENERAL PURPOSE I/O PORT REGISTER

typedef enum REG_GPIO {

  GP0 = 0x01,
  GP1 = 0x02,
  GP2 = 0x04,
  GP3 = 0x08,
  GP4 = 0x10,
  GP5 = 0x20,
  GP6 = 0x40,
  GP7 = 0x80

} REG_GPIO;

// OUTPUT LATCH REGISTER

typedef enum REG_OLAT {

  OL0 = 0x01,
  OL1 = 0x02,
  OL2 = 0x04,
  OL3 = 0x08,
  OL4 = 0x10,
  OL5 = 0x20,
  OL6 = 0x40,
  OL7 = 0x80

} REG_OLAT;
// *****************************************************************************

//FUNCTION PROTOTYPES

void
Get_mcp23s08_GPIOs_PinMode(uint8_t *IOSta);
uint8_t
Get_mcp23s08_GPIO_PinMode(uint8_t PinNo);
void
Set_mcp23s08_GPIO_PinMode (uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_GPIO_PinMode_OUT (REG_IO value);
void
Get_mcp23s08_GPIO_STAT_Polarity (uint8_t *IPSta);
uint8_t
Get_mcp23s08_GPIO_Polarity(uint8_t PinNo);
void
Set_mcp23s08_GPIO_Polarity  (uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_GPIO_ACTIVE_LOW(REG_IPOL value);
void
Get_mcp23s08_GPIOsta_INTR_PinChange(uint8_t *GPINTSta);
uint8_t
Get_mcp23s08_GPIO_INTR_PinChange(uint8_t PinNo);
void
Set_mcp23s08_GPIO_INTR_PinChange(uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_GPIO_Multi_INTR_PinChange (REG_GPINT value);
void
Get_mcp23s08_GPIOsta_DEFAULT_COMPARE(uint8_t *DEFVALSta);
uint8_t
Get_mcp23s08_GPIO_DEFAULT_COMPARE(uint8_t PinNo);
void
Set_mcp23s08_GPIO_DEFAULT_COMPARE(uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_GPIO_DEFAULT_COMPARE_HIGH (REG_DEF value);
void
Get_mcp23s08_GPIOsta_INTR_CONTROL(uint8_t *INTCONSta);
uint8_t
Get_mcp23s08_GPIO_INTR_CONTROL(uint8_t PinNo);
void
Set_mcp23s08_GPIO_INTR_CONTROL(uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_GPIO_INTR_CONTROL_DEFVAL (REG_INTCON value);
uint8_t
Get_mcp23s08_Sequential_Operation_Mode();

//0-sequential mode and 1-Byte mode
void
Set_mcp23s08_Sequential_Operation_Mode (uint8_t value);
uint8_t
Get_mcp23s08_Slew_Rate_Control();
void
Set_mcp23s08_Slew_Rate_Control (uint8_t value);
uint8_t
Get_mcp23s08_Address_Pins_stat();
void
Set_mcp23s08_Address_Pins (uint8_t value);
uint8_t
Get_mcp23s08_INT_pin_config();
void
Set_mcp23s08_INT_pin_config (uint8_t value);
uint8_t
Get_mcp23s08_INT_polarity();
void
Set_mcp23s08_INT_polarity(uint8_t value);
void
Get_mcp23s08_GPIOsta_PULL_UP(uint8_t *GPPUSta);
uint8_t
Get_mcp23s08_GPIO_PULL_UP_Config(uint8_t PinNo);
void
Set_mcp23s08_GPIO_PULL_UP(uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_GPIO_PULL_UP_EN (REG_GPPU value);
void
Get_mcp23s08_INTR_FLAG_STAT(uint8_t *INTSta);
uint8_t
Get_mcp23s08_GPIO_INTR_Config(uint8_t PinNo);
void
Get_mcp23s08_INTR_CAPTURE_Config(uint8_t *ICPSta);
uint8_t
Get_mcp23s08_GPIO_INTR_CAPTURE_Config(uint8_t PinNo);
void
Get_mcp23s08_GPIO_Reg (uint8_t *GPIOSta);
uint8_t
Get_mcp23s08_GPIO_Data(uint8_t PinNo);
void
Set_mcp23s08_GPIO (uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_GPIO_High(REG_GPIO value);
void
Get_mcp23s08_sta_OUTPUT_LATCH(uint8_t *GPPUSta);
uint8_t
Get_mcp23s08_OUTPUT_LATCH_Config(uint8_t PinNo);
void
Set_mcp23s08_OUTPUT_LATCH(uint8_t PinNo, uint8_t value);
void
Set_mcp23s08_OUTPUT_LATCH_Reg(REG_OLAT value);
void
Get_mcp23s08_GPIO_Pinstat (uint8_t PinNo);
void
Set_mcp23s08_GPIO_INTR_Handle (uint8_t PinNo, uint8_t defval, uint8_t intcon);
#endif /* SRC_MCP23S08_H_ */
