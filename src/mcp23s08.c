/*******************************************************************************
 MCP23S08 Driver:  Implementation

 File Name:
 mcp23s08.c

 Summary:

 This Source file provides Implementation of the all Register Level Functions
 of the MCP23S08 I/O Expander.

 Description:
 None.
 *******************************************************************************/

// Includes
#include "mcp23s08.h"
#include "spi.h"

// *****************************************************************************

// SPI Register level functions

/*REG_IODIR
 Get_mcp23s08_GPIOs_PinMode ()
 {

 uint8_t rd, Reg_addr = MCP23S08_IODIR_A;

 rd = SPI_ReadByte (Reg_addr);

 REG_IODIR *IODIR = (REG_IODIR*) &rd;

 return *IODIR;
 }*/

// *****************************************************************************

/* This Function provides Input and output mode status of all GPIO Pins
 * Status is determined using reference #IOSta*/
void
Get_mcp23s08_GPIOs_PinMode (uint8_t *IOSta)
{

  uint8_t rd, Reg_addr = MCP23S08_IODIR_A;

  rd = SPI_ReadByte (Reg_addr);

  *IOSta = rd;

}

/* This Function provides Input and output mode status of specified pin.
 * Send the pin number using reference #PinNo (0,1,2,3,4,5,6,7)*/
uint8_t
Get_mcp23s08_GPIO_PinMode (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_IODIR_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

/* This Function is for setting Input and output mode status of specified pin.
 * Send the pin number using reference #PinNo (0,1,2,3,4,5,6,7)
 * Send the value of pin using reference #value*/
void
Set_mcp23s08_GPIO_PinMode (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_IODIR_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}

void
Set_mcp23s08_GPIO_PinMode_OUT (REG_IO value)
{

  uint8_t val, Reg_addr = MCP23S08_IODIR_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

void
Get_mcp23s08_GPIO_STAT_Polarity (uint8_t *IPSta)
{

  uint8_t rd, Reg_addr = MCP23S08_IPOL_A;

  rd = SPI_ReadByte (Reg_addr);

  *IPSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_Polarity (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_IPOL_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Set_mcp23s08_GPIO_Polarity (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_IPOL_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}

void
Set_mcp23s08_GPIO_ACTIVE_LOW (REG_IPOL value)
{

  uint8_t val, Reg_addr = MCP23S08_IPOL_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

void
Get_mcp23s08_GPIOsta_INTR_PinChange (uint8_t *GPINTSta)
{

  uint8_t rd, Reg_addr = MCP23S08_GPINTEN_A;

  rd = SPI_ReadByte (Reg_addr);

  *GPINTSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_INTR_PinChange (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_GPINTEN_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Set_mcp23s08_GPIO_INTR_PinChange (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_GPINTEN_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}



void
Set_mcp23s08_GPIO_Multi_INTR_PinChange (REG_GPINT value)
{

  uint8_t val, Reg_addr = MCP23S08_GPINTEN_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

void
Get_mcp23s08_GPIOsta_DEFAULT_COMPARE (uint8_t *DEFVALSta)
{

  uint8_t rd, Reg_addr = MCP23S08_DEFVAL_A;

  rd = SPI_ReadByte (Reg_addr);

  *DEFVALSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_DEFAULT_COMPARE (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_DEFVAL_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Set_mcp23s08_GPIO_DEFAULT_COMPARE (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_DEFVAL_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}

void
Set_mcp23s08_GPIO_DEFAULT_COMPARE_HIGH (REG_DEF value)
{

  uint8_t val, Reg_addr = MCP23S08_DEFVAL_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

void
Get_mcp23s08_GPIOsta_INTR_CONTROL (uint8_t *INTCONSta)
{

  uint8_t rd, Reg_addr = MCP23S08_INTCON_A;

  rd = SPI_ReadByte (Reg_addr);

  *INTCONSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_INTR_CONTROL (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_INTCON_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Set_mcp23s08_GPIO_INTR_CONTROL (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_INTCON_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}

void
Set_mcp23s08_GPIO_INTR_CONTROL_DEFVAL (REG_INTCON value)
{

  uint8_t val, Reg_addr = MCP23S08_INTCON_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

uint8_t
Get_mcp23s08_Sequential_Operation_Mode ()
{

  uint8_t rd, val, Reg_addr = MCP23S08_IOCON_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> 5) & 0x01;

  return val;

}

void
Set_mcp23s08_Sequential_Operation_Mode (uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_IOCON_A;

  val &= ~(0x20);

  val |= (value << 5) & 0x20;

  SPI_WriteByte (Reg_addr, val);

}

uint8_t
Get_mcp23s08_Slew_Rate_Control ()
{

  uint8_t rd, val, Reg_addr = MCP23S08_IOCON_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> 4) & 0x01;

  return val;

}

void
Set_mcp23s08_Slew_Rate_Control (uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_IOCON_A;

  val &= ~(0x10);

  val |= (value << 4) & 0x10;

  SPI_WriteByte (Reg_addr, val);

}

uint8_t
Get_mcp23s08_Address_Pins_stat ()
{

  uint8_t rd, val, Reg_addr = MCP23S08_IOCON_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> 3) & 0x01;

  return val;

}

void
Set_mcp23s08_Address_Pins (uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_IOCON_A;

  val &= ~(0x08);

  val |= (value << 3) & 0x08;

  SPI_WriteByte (Reg_addr, val);

}

uint8_t
Get_mcp23s08_INT_pin_config ()
{

  uint8_t rd, val, Reg_addr = MCP23S08_IOCON_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> 2) & 0x01;

  return val;

}

void
Set_mcp23s08_INT_pin_config (uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_IOCON_A;

  val &= ~(0x04);

  val |= (value << 2) & 0x04;

  SPI_WriteByte (Reg_addr, val);


}

uint8_t
Get_mcp23s08_INT_polarity ()
{

  uint8_t rd, val, Reg_addr = MCP23S08_IOCON_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> 1) & 0x01;

  return val;

}

void
Set_mcp23s08_INT_polarity (uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_IOCON_A;

  val &= ~(0x02);

  val |= (value << 2) & 0x02;

  SPI_WriteByte (Reg_addr, val);

  Set_mcp23s08_INT_pin_config (0x00);


}

void
Get_mcp23s08_GPIOsta_PULL_UP (uint8_t *GPPUSta)
{

  uint8_t rd, Reg_addr = MCP23S08_GPPU_A;

  rd = SPI_ReadByte (Reg_addr);

  *GPPUSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_PULL_UP_Config (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_GPPU_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Set_mcp23s08_GPIO_PULL_UP (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_GPPU_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}

void
Set_mcp23s08_GPIO_PULL_UP_EN (REG_GPPU value)
{

  uint8_t val, Reg_addr = MCP23S08_GPPU_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

void
Get_mcp23s08_INTR_FLAG_STAT (uint8_t *INTSta)
{

  uint8_t rd, Reg_addr = MCP23S08_INTF_A;

  rd = SPI_ReadByte (Reg_addr);

  *INTSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_INTR_Config (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_INTF_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Get_mcp23s08_INTR_CAPTURE_Config (uint8_t *ICPSta)
{

  uint8_t rd, Reg_addr = MCP23S08_INTCAP_A;

  rd = SPI_ReadByte (Reg_addr);

  *ICPSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_INTR_CAPTURE_Config (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_INTCAP_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Get_mcp23s08_GPIO_Reg (uint8_t *GPIOSta)
{

  uint8_t rd, Reg_addr = MCP23S08_GPIO_A;

  rd = SPI_ReadByte (Reg_addr);

  *GPIOSta = rd;

}

uint8_t
Get_mcp23s08_GPIO_Data (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_GPIO_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Set_mcp23s08_GPIO (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_GPIO_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}

void
Set_mcp23s08_GPIO_High (REG_GPIO value)
{

  uint8_t val, Reg_addr = MCP23S08_GPIO_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

void
Get_mcp23s08_sta_OUTPUT_LATCH (uint8_t *GPPUSta)
{

  uint8_t rd, Reg_addr = MCP23S08_OLAT_A;

  rd = SPI_ReadByte (Reg_addr);

  *GPPUSta = rd;

}

uint8_t
Get_mcp23s08_OUTPUT_LATCH_Config (uint8_t PinNo)
{

  uint8_t rd, val, Reg_addr = MCP23S08_OLAT_A;

  rd = SPI_ReadByte (Reg_addr);

  val &= ~(0x01);

  val |= (rd >> PinNo) & 0x01;

  return val;

}

void
Set_mcp23s08_OUTPUT_LATCH (uint8_t PinNo, uint8_t value)
{

  uint8_t val, Reg_addr = MCP23S08_OLAT_A;

  val &= ~(2 ^ PinNo);

  val |= (value << PinNo) & (2 ^ PinNo);

  SPI_WriteByte (Reg_addr, val);

}

void
Set_mcp23s08_OUTPUT_LATCH_Reg (REG_OLAT value)
{

  uint8_t val, Reg_addr = MCP23S08_OLAT_A;

  val &= ~(0xff);

  val |= value & 0xff;

  SPI_WriteByte (Reg_addr, val);

}

void
Get_mcp23s08_GPIO_Pinstat (uint8_t PinNo)
{

  PIN_STATUS PinSta;

  PinSta.PinMode = Get_mcp23s08_GPIO_PinMode (PinNo);
  PinSta.Polarity = Get_mcp23s08_GPIO_Polarity (PinNo);
  PinSta.INTR_CHG = Get_mcp23s08_GPIO_INTR_PinChange (PinNo);
  PinSta.DEFVAL = Get_mcp23s08_GPIO_DEFAULT_COMPARE (PinNo);
  PinSta.INTCON = Get_mcp23s08_GPIO_INTR_CONTROL (PinNo);
  PinSta.GPIO = Get_mcp23s08_GPIO_Data (PinNo);
  if (PinSta.PinMode == 1)
    {

      PinSta.GPPU = Get_mcp23s08_GPIO_PULL_UP_Config (PinNo);
    }
  else
    {
      PinSta.OLAT = Get_mcp23s08_OUTPUT_LATCH_Config (PinNo);
    }

}

void
Set_mcp23s08_GPIO_INTR_Handle (uint8_t PinNo, uint8_t defval, uint8_t intcon)
{

  Set_mcp23s08_GPIO_INTR_PinChange (PinNo, 0x01);
  Set_mcp23s08_GPIO_DEFAULT_COMPARE (PinNo, defval);
  Set_mcp23s08_GPIO_INTR_CONTROL (PinNo, intcon);

}

