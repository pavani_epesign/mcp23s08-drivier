/*******************************************************************************
  SPI Driver:  Header File

  File Name:
    spi.h

  Summary:
    This header file contains the MCU specific SPI definitions and declarations.

  Description:
    .
 *******************************************************************************/

#ifndef SRC_SPI_H_
#define SRC_SPI_H_


#define CLIENT_ADDR_RD        0x41    // Client address for reading
#define CLIENT_ADDR_WR        0x40    // Client address for writing


#define MAX_BUFFER_SIZE          8    // Maximum buffer Master will send



// SPI Functions

// *****************************************************************************


/* SPI Byte Read
 * Reading one Byte from  register #Reg_addr
 */
uint8_t
SPI_ReadByte (uint8_t Reg_addr);


/* SPI Read Array
 * Reading array of data of size #Byte_RD from #Reg_addr.
 * Updating data in #RxData
 */
void
SPI_ReadArray (uint8_t Reg_addr, uint8_t Byte_RD, uint8_t *RxData);



/* SPI Byte Write
 * Writing one Byte to register #Reg_addr
 */
void
SPI_WriteByte (uint8_t Reg_addr, uint8_t Txdata);


/* SPI Write Array
 * Writing array of data #Txdata of #Byte_WR to the register #Reg_addr
 */
void
SPI_WriteArray (uint8_t Reg_addr, uint8_t Byte_WR, uint8_t *Txdata);








#endif /* SRC_SPI_H_ */
